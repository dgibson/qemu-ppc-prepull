#! /bin/sh

set -ex

git clone $QEMU_GIT_REPO --branch $QEMU_GIT_BRANCH

mkdir -p qemu/build/$VARIANT
cd qemu/build/$VARIANT

../../configure --cross-prefix=$CC_PREFIX

make -j8

ls -l qemu-img.exe
file qemu-img.exe
