#! /bin/bash

# Need bash for the "$@" in build.sh"

set -ex

. ./build.sh "--target-list=x86_64-linux-user,ppc-linux-user,ppc64le-linux-user,ppc64-linux-user"

make check-tcg
