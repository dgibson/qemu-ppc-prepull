#! /bin/bassh

set -ex

git clone $QEMU_GIT_REPO --branch $QEMU_GIT_BRANCH

mkdir -p qemu/build/$VARIANT
cd qemu/build/$VARIANT

../../configure "$@"

make -j8
