NAME = qemu-ppc-prepull
REPO = quay.io/dwg

QEMU_GIT_REPO = https://gitlab.com/dgibson/qemu.git
QEMU_GIT_BRANCH = ppc-for-6.2

IMGPREFIX = $(REPO)/$(NAME)

PODMAN = podman
TAG=$(shell git describe --tags --dirty)

VARIANTS = win32 win64 \
	x86-min-check x86-min-check-acceptance \
	x86-full-check \
	x86-host-tcg
INTERMEDIATES = base win-base x86-minimal x86-full

POD_YAMLS = $(VARIANTS:%=%/pod.yaml)

all: allpods.yaml

allpods.yaml: $(POD_YAMLS)
	cat $^ > $@

images: $(VARIANTS:%=image-%)

%/pod.yaml: pod.yaml.in push-%
	sed 's!@VARIANT@!$*!;s!@IMGPREFIX@!$(IMGPREFIX)!;s!@NAME@!$(NAME)!;s!@VERSION@!$(TAG)!' < $< > $@

%/Dockerfile: %/Dockerfile.in
	sed 's!@QEMU_GIT_REPO@!$(QEMU_GIT_REPO)!;s!@QEMU_GIT_BRANCH@!$(QEMU_GIT_BRANCH)!' < $< > $@

image-%: %/Dockerfile
	$(PODMAN) build -t $(IMGPREFIX)-$*:$(TAG) $* --build-arg VERSION=$(TAG) --build-arg IMGPREFIX=$(IMGPREFIX)

$(VARIANTS:%=image-%): image-base

push: $(VARIANTS:%=push-%) $(INTERMEDIATES:%=push-%)

tag-%: image-%
	$(PODMAN) tag $(IMGPREFIX)-$*:$(TAG) $(IMGPREFIX)-$*:latest

push-%: tag-%
	$(PODMAN) push $(IMGPREFIX)-$*:$(TAG)
	$(PODMAN) push $(IMGPREFIX)-$*:latest

clean-image-%:
	$(PODMAN) rmi $(IMGPREFIX)/$(NAME)-$*

run-%: image-%
	$(PODMAN) run -it $(NAME)-$*:$(TAG)

enter-%: image-%
	$(PODMAN) run -it $(NAME)-$*:$(TAG) /bin/bash

clean:
	rm -f *~
	rm -f */*~
	rm -f allpods.yaml $(POD_YAMLS)
	rm -f base/Dockerfile

realclean: $(VARIANTS:%=clean-image-%) $(INTERMEDIATES:%=clean-image-%)

image-win-base: tag-base
image-win32: tag-win-base
image-win64: tag-win-base
image-x86-minimal: tag-base
image-x86-min-check: tag-x86-minimal
image-x86-min-check-acceptance: tag-x86-minimal
image-x86-full-check: tag-x86-full

killpods:
	oc delete all -l app=qemu-ppc-prepull
