#! /bin/sh

set -ex

cd /root

git clone $QEMU_GIT_REPO --branch $QEMU_GIT_BRANCH
mkdir -p qemu/build/win64
cd qemu/build/win64

../../configure --cross-prefix=x86_64-w64-mingw32-

make -j8

ls -l qemu-img.exe
file qemu-img.exe
